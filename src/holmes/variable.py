class Variable(object):
    def __init__(self, name):
        self.type = "variable"
        self.name = name

    def __str__(self):
        return "%s"%(self.name,)

    def __repr__(self):
        if hasattr(self, "_truthValue"):
            return "%s(%s)"%(self.name,self.truthValue)
        else:
            return "%s"%(self.name,)

    @property
    def truthValue(self):
        if not hasattr(self, "_truthValue"):
            raise Exception("Truth value not assigned for %s"%self.name)
        return self._truthValue

    @truthValue.setter
    def truthValue(self, value):
        self._truthValue = value

    def _evaluate(self):
        return self.truthValue
