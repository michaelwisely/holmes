from ply import lex
from ply import yacc

from connectives import VariableExpression, Conjunction, Disjunction
from connectives import Condition, Bicondition, Negation
from variable import Variable

import os

tokens = (
    "VARIABLE",
    "NEGATION",
    "CONJUNCT",
    "DISJUNCT",
    "COND",
    "BICOND",
    "LPAREN",
    "RPAREN",
    )

# Regular expressions for SL tokens
t_VARIABLE = r'[A-Z]'
t_NEGATION = r"\~"
t_CONJUNCT = r'&'
t_DISJUNCT = r'v'
t_COND     = r'->'
t_BICOND   = r'='
t_LPAREN   = r'\('
t_RPAREN   = r'\)'

# Ignore whitespace
t_ignore   = "[ \r\n\t]+"

def t_error(t):
    raise TypeError("Unknown text '%s'" % (t.value,))

lex.lex()

def p_s(p):
    """
    s : sentence
    """
    p[0] = p[1]

def p_sentence(p):
    """
    sentence : expr
    sentence : complex_sentence
    """
    if isinstance(p[1], Variable):
        p[0] = VariableExpression(p[1])
    else:
        p[0] = p[1]

def p_expr(p):
    """
    expr : VARIABLE
    expr : NEGATION expr
    expr : LPAREN sentence RPAREN
    """
    # Got a variable
    if len(p) == 2:
        p[0] = Variable(p[1])

    # Got a negated expression
    elif len(p) == 3:
        p[0] = Negation(p[2])

    # Got a plain ol' sentence.
    else:
        p[0] = p[2]

def p_complex_sentence(p):
    """
    complex_sentence : expr connective expr
    """
    if p[2] == t_CONJUNCT:
        p[0] = Conjunction(p[1], p[3])
    elif p[2] == t_DISJUNCT:
        p[0] = Disjunction(p[1], p[3])
    elif p[2] == t_COND:
        p[0] = Condition(p[1], p[3])
    elif p[2] == t_BICOND:
        p[0] = Bicondition(p[1], p[3])
    else:
        raise Exception("Bad connective: %s"%p[2])


def p_connective(p):
    """
    connective : CONJUNCT
    connective : DISJUNCT
    connective : COND
    connective : BICOND
    """
    p[0] = p[1]

def p_error(p):
    raise TypeError("unknown text at %r" % (p.value,))

yacc.yacc(debug=0, outputdir=os.path.dirname(__file__))

class SL_Parser(object):
    def __init__(self):
        self._parse = yacc.parse

    def parse(self, sentence_string):
        return self._parse(sentence_string)

if __name__ == "__main__":
    parser = SL_Parser()
    tree = parser.parse("(~A->D)&((BvC)=D)")
    print tree.unique_variables

    tree.assignVariables(A=True, B=False, C=True, D=False)

    print tree.evaluate(A=True, B=True, C=True, D=False)
