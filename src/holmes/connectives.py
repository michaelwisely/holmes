from variable import Variable 

class Expression(object):
    """Abstract class. Don't instantiate. Just inherit."""

    def __repr__(self):
        return str(self)

    def evaluate(self, **kwargs):
        var_names = map(str, self.variables)
        self.assignVariables(**kwargs)
        return self._evaluate()

    def _evaluate(self):
        raise Exception("Unimplemnted for class %s"%(type(self), ))

    def assignVariables(self, **kwargs):
        if not all([type(x) == bool for x in  kwargs.values()]):
            raise Exception("All truth assignments must be True or False")
        # if any([x not in self.unique_variables for x in kwargs.keys()]):
        #     bad = ', '.join(set(kwargs.keys())-set(self.unique_variables))
        #     raise Exception("Variables not in sentence: %s"%bad)
        for variable in self.variables:
            if variable.name in kwargs:
                variable.truthValue = kwargs[variable.name]

    @property
    def unique_variables(self):
        return list(set(map(str, self.variables)))

    @property
    def variables(self):
        v_set = []
        if hasattr(self, "left"):
            if isinstance(self.left, Variable):
                v_set.append(self.left)
            elif isinstance(self.left, Expression):
                v_set.extend(self.left.variables)
        if hasattr(self, "right"):
            if isinstance(self.right, Variable):
                v_set.append(self.right)
            elif isinstance(self.right, Expression):
                v_set.extend(self.right.variables)
        return v_set

class VariableExpression(Expression):
    def __init__(self, right):
        self.type = "expression"
        self.right = right

    def __str__(self):
        return str(self.right)

    def _evaluate(self):
        return self.right._evaluate()

class Conjunction(Expression):
    def __init__(self,left,right):
        self.type = "conjunction"
        self.left = left
        self.right = right

    def __str__(self):
        return "(%s & %s)"%(str(self.left), str(self.right))

    def _evaluate(self):
        return self.left._evaluate() and self.right._evaluate()

class Disjunction(Expression):
    def __init__(self,left,right):
        self.type = "disjunction"
        self.left = left
        self.right = right

    def __str__(self):
        return "(%s v %s)"%(str(self.left), str(self.right))

    def _evaluate(self):
        return self.left._evaluate() or self.right._evaluate()


class Condition(Expression):
    def __init__(self,left,right):
        self.type = "condition"
        self.left = left
        self.right = right

    def __str__(self):
        return "(%s -> %s)"%(str(self.left), str(self.right))

    def _evaluate(self):
        return (not self.left._evaluate()) or self.right._evaluate()


class Bicondition(Expression):
    def __init__(self,left,right):
        self.type = "bicondition"
        self.left = left
        self.right = right

    def __str__(self):
        return "(%s = %s)"%(str(self.left), str(self.right))

    def _evaluate(self):
        both = self.left._evaluate() and self.right._evaluate()
        neither = (not self.left._evaluate()) and (not self.right._evaluate())
        return both or neither


class Negation(Expression):
    def __init__(self,right):
        self.type = "negation"
        self.right = right

    def __str__(self):
        return "~%s"%str(self.right)

    def _evaluate(self):
        return (not self.right._evaluate() )
