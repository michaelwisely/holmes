from parser import SL_Parser

import argparse
import sys

def truth_value_combinations(num_variables):
    counter = 2**num_variables-1
    while counter >= 0:
        yield [x == '1' for x in bin(counter)[2:].rjust(num_variables, '0')]
        counter -= 1



def check_logical_consistency(premises, conclusion):
    sentences = premises + [conclusion]
    variables = reduce(lambda acc, x: acc.union(set(x.unique_variables)),
                       sentences, set())
    truthValues = truth_value_combinations(len(variables))
    for combination in truthValues:
        if all([x.evaluate(**dict(zip(variables, combination))) for x in sentences]):
            return True
    return False


def check_logical_validity(tree):
    pass

def main():

    parser = argparse.ArgumentParser(description='Check a file for logical consistency.')
    parser.add_argument('filename', type=str, help='the file to process')
    parser.add_argument('--check_consistency', action='store_true', help='check for logical consistency')

    args = parser.parse_args()

    lines_from_file = None
    try:
        with open(args.filename) as f:
            # Ditch the lines that are blank.
            lines_from_file = [l.strip() for l in f.readlines() if l.strip() != '']
    except IOError:
        sys.exit("Couldn't open %s"%args.filename)

    parser = SL_Parser()
    premises = []
    for line in lines_from_file[:-1]:
        premises.append(parser.parse(line))

    conclusion = parser.parse(lines_from_file[-1])

    print "Parsed the following argument:"
    print "Premises"
    for premise in premises:
        print "\t", premise

    print "Conclusion"
    print "\t", conclusion

    if args.check_consistency:
        print "Checking consistency..."
        print "\tThe argument is",
        if check_logical_consistency(premises, conclusion):
            print "valid"
        else:
            print "invalid"


if __name__ == '__main__':
    main()
