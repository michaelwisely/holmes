import unittest
from holmes.parser import SL_Parser

class TestParser(unittest.TestCase):
    def setUp(self):
        self.parser = SL_Parser()

    def test_variable(self):
        tree = self.parser.parse("A")
        self.assertNotIn("B", tree.unique_variables)

        truthTable = [({'A':True},  True),
                      ({'A':False}, False),
                      ]
        
        self.assertTruthValues(tree, truthTable)

    def assertTruthValues(self, tree, truthList):
        """
        [({'a':True, 'b':True}, True), ...]
        """
        for assignments, expected in truthList:
            self.assertEqual(tree.evaluate(**assignments), expected)

    def test_conjunction(self):
        tree = self.parser.parse("A & B")
        self.assertIn("A", tree.unique_variables)
        self.assertIn("B", tree.unique_variables)

        truthTable = [({'A':True,  'B': True},  True),
                      ({'A':True,  'B': False}, False),
                      ({'A':False, 'B': True},  False),
                      ({'A':False, 'B': False}, False)
                      ]
        
        self.assertTruthValues(tree, truthTable)

    def test_disjunction(self):
        tree = self.parser.parse("A v B")
        self.assertIn("A", tree.unique_variables)
        self.assertIn("B", tree.unique_variables)

        truthTable = [({'A':True,  'B': True},  True),
                      ({'A':True,  'B': False}, True),
                      ({'A':False, 'B': True},  True),
                      ({'A':False, 'B': False}, False)
                      ]
        
        self.assertTruthValues(tree, truthTable)

    def test_condition(self):
        tree = self.parser.parse("A -> B")
        self.assertIn("A", tree.unique_variables)
        self.assertIn("B", tree.unique_variables)

        truthTable = [({'A':True,  'B': True},  True),
                      ({'A':True,  'B': False}, False),
                      ({'A':False, 'B': True},  True),
                      ({'A':False, 'B': False}, True)
                      ]
        
        self.assertTruthValues(tree, truthTable)

    def test_bicondition(self):
        tree = self.parser.parse("A = B")
        self.assertIn("A", tree.unique_variables)
        self.assertIn("B", tree.unique_variables)

        truthTable = [({'A':True,  'B': True},  True),
                      ({'A':True,  'B': False}, False),
                      ({'A':False, 'B': True},  False),
                      ({'A':False, 'B': False}, True)
                      ]
        
        self.assertTruthValues(tree, truthTable)

    def test_negation(self):
        tree = self.parser.parse("~A")
        self.assertIn("A", tree.unique_variables)
        self.assertNotIn("B", tree.unique_variables)

        truthTable = [({'A':True},  False),
                      ({'A':False}, True),
                      ]
        
        self.assertTruthValues(tree, truthTable)

if __name__ == '__main__':
    unittest.main()
