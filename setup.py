from setuptools import setup, find_packages

setup(
    name='holmes',
    description='An SL logic library',
    classifiers=[],
    author='Michael Wisely',
    author_email='michaelwisely@gmail.com',
    package_dir={'': 'src'},
    packages=find_packages('src'),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'ply',
        'argparse',
    ],
    entry_points={
        'console_scripts': ['holmes = holmes.holmes:main'],
        },
)
